// struct CreateFileParams {
//     lpFileName: LPCSTR,
//     dwDesiredAccess: DWORD,
//     dwShareMode: DWORD,
//     lpSecurityAttributes: LPSECURITY_ATTRIBUTES,
//     dwCreationDisposition: DWORD,
//     dwFlagsAndAttributes: DWORD,
//     hTemplateFile: HANDLE,
// }

// struct CreateFileReturns(HANDLE);

// extern "system" {
//     pub fn CreateFile(params: CreateFileParams) -> CreateFileReturns;

// }

// pub struct CreateFileFn(fn(CreateFileParams) -> CreateFileReturns);

// pub enum VTable {
//     CreateFile(fn(CreateFileParams) -> CreateFileReturns),
// }

// impl VTable {
//     pub fn call(&self) -> VTable {
//         use VTable::*;
//         match *self {
//             CreateFile(function) => {
//                 let called = (function)(CreateFileA);
//                 called.0;
//                 //     let n = CreateFileA(
//                 //         params.,
//                 //         dwDesiredAccess,
//                 //         dwShareMode,
//                 //         lpSecurityAttributes,
//                 //         dwCreationDisposition,
//                 //         dwFlagsAndAttributes,
//                 //         hTemplateFile,
//                 // )
//             }
//         }
//     }
// }
