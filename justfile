# Use bash on MacOS and Linux
set shell := ["bash", "-cu"]
# Use PowerShell on Windows
set windows-shell := ["pwsh", "-NoLogo", "-Command"]

# Load `.env` file if present
set dotenv-load := true

# Uncomment the following line to view verbose backtraces:
#export RUST_BACKTRACE := "1"

# Default to show all available commands if no arguments passed
_default:
    @just --list

# Create an optimized 'release' build
@build:
    cargo build --release --verbose

# Sanity check to ensure the project compiles
@check:
    cargo +nightly fmt --all -- --check
    cargo test --workspace -- --quiet
    cargo +nightly clippy --workspace --all-targets -- -D warnings

# Quickly format and run linter
@lint:
    cargo +nightly clippy --workspace --all-targets

# Run performance benchmarks
@bench:
    cargo bench --verbose

# Create an HTML chart showing compilation timings
@timings:
    cargo clean
    cargo build -Z timings

## Testing
# Run unit tests
@test:
    cargo test --workspace -- --quiet

# Run all unit tests (in release mode)
@test-release:
    cargo test --workspace --release --verbose

# Run tests sequentially, with parallelism disabled
@test-debug:
    cargo test --workspace -- --nocapture --test-threads=1

# Run tests without capturing stdout/sterr
@test-verbose:
    cargo test --workspace -- --nocapture

## Project management
# Build the crate documentation, failing on any errors
@generate-docs:
    cargo doc --no-deps --document-private-items --all-features --workspace --verbose --open

# Cleans the project sources, then rebuilds them
@refresh:
    cargo clean --verbose
    cargo build

# Show the versions of required build tools
@versions:
    rustc --version
    cargo --version

## Running
# Run the default binary
@run args:
    cargo run -- {{ args }}

# Start the server
@start-client:
    cargo run --bin lucid-client

# Start the server
@start-server:
    cargo run --bin lucid-server

# Start both the client and server
@start:
    just start-server
    just start-client